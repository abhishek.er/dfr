from django.urls import path

from snippets.views import SnippetListGCBV, SnippetDetailGCBV, UserList, UserDetail


urlpatterns = [
	path('users/', UserList.as_view()),
	path('users/<int:pk>/', UserDetail.as_view()),
	path('snippets/', SnippetListGCBV.as_view()),
	path('snippets/<int:pk>/', SnippetDetailGCBV.as_view()),

]