from django.contrib import admin
from posts.models import Post, Report


admin.site.register(Post)
admin.site.register(Report)