from fpdf import FPDF

from django.contrib.auth import get_user_model
from django.shortcuts import render
from django.http import HttpResponse

from rest_framework import generics
from rest_framework import filters
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.views import APIView

from posts.models import Post
from posts.permissions import IsAuthorOrReadOnly
from posts.serializers import PostSerializer, UserSerializer



class PostList(generics.ListCreateAPIView):
    """
    """

    queryset = Post.objects.all()
    serializer_class = PostSerializer


class PostDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    """
    permission_classes = (IsAuthorOrReadOnly,)
    queryset = Post.objects.all()
    serializer_class = PostSerializer


class UserList(generics.ListCreateAPIView):
    """
    """
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ['username'] # set field search agaings
    # set which field may be ordered against
    ordering_fields = ['id','username']
    ordering = ['username'] # set default ordering


class UserDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    """
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer


class PostViewSet(viewsets.ModelViewSet):
    """
    """
    permission_classes = (IsAuthorOrReadOnly,)
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter)
    search_fields = ['title','author__username']
    ordering_fields = ['id', 'title']


class UserViewSet(viewsets.ModelViewSet):
    """
    """
    queryset = get_user_model().objects.all()
    serializer_class = UserSerializer
    filter_backends = (filters.SearchFilter, filters.OrderingFilter,)
    search_fields = ['username']
    ordering_fields = ['id', 'username']


class PDF(FPDF):
    def header(self):
        # Logo
        # self.image('logo_pb.png', 10, 8, 33)
        # Arial bold 15
        self.set_font('Arial', 'B', 15)
        # Move to the right
        self.cell(80)
        # Title
        self.cell(30, 10, 'Title', 1, 0, 'C')
        # Line break
        self.ln(20)

    # Page footer
    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()) + '/{nb}', 0, 0, 'C')


class DownloadPdfView(APIView):

    def get(self, *args, **kwargs):
        
        pdf = PDF()
        pdf.alias_nb_pages()
        pdf.add_page()
        pdf.set_font('Times', '', 12)
        for i in range(1, 41):
            pdf.cell(0, 10, 'Printing line number ' + str(i), 0, 1)
        pdf.output('tuto2.pdf', 'D')
        response = HttpResponse(content_type='application/pdf')
        response['Content-Disposition'] = 'inline; filename="tuto2.pdf"'
        # response=HttpResponse('tuto2.pdf',content_type='application/pdf')
        # response['Content-Disposition']='filename="test.pdf"'
        return response